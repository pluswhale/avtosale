
import React from "react";

class Modal extends React.Component {
  componentDidMount() {
    var modal = document.getElementById("modal");
    var modalContent = document.querySelector(".modal-content");
    var loginLink = document.getElementById("login-link");
    var registerLink = document.getElementById("register-link");

    if (loginLink && registerLink) {
      loginLink.addEventListener("click", function (event) {
        event.preventDefault();
        modal.style.display = "block";
      });

      registerLink.addEventListener("click", function (event) {
        event.preventDefault();
        modal.style.display = "block";
      });
    }

    modal.addEventListener("click", function (event) {
      if (event.target === modal || event.target === modalContent) {
        modal.style.display = "none";
      }
    });
  }

  render() {
    return (
      <div id="modal" className="modal">
        <div className="modal-content">
          <h2>Войти на сайт</h2>
          <label htmlFor="email">Email:</label>
          <input type="email" id="email" />
          <label htmlFor="password">Пароль:</label>
          <input type="password" id="password" />
          <button type="submit">Войти</button>
          <p>
            Не зарегистрированы? <a href="/">Зарегистрируйтесь</a>
          </p>
        </div>
      </div>
    );
  }
}

export default Modal;

