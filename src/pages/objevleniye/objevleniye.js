import React from "react";
import "../../css/objevleniye.css";

class CarListing extends React.Component {
  render() {
    return (
      <div className="car-listing">
        <div className="car-img">
          <img src="car-image.jpg" alt="Car" />
        </div>
        <div className="car-details">
          <h3 className="car-title">Марка и модель автомобиля</h3>
          <p className="car-price">Цена: $30,000</p>
          <p className="car-location">Местоположение: Москва</p>
          <p className="car-description">
            Описание автомобиля и другие детали.
            Описание автомобиля и другие детали.
          </p>
        </div>
      </div>
    );
  }
}

export default CarListing;
