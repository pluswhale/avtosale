import React from 'react';

const AboutPage = () => {
  return (
    <div>
      <h1>О нас</h1>
      <p>Мы занимаемся разработкой проекта на React-Redux!</p>
    </div>
  );
};

export default AboutPage;

