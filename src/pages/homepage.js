import React from 'react';
import avtologo from '../img/Без названия.png';
import '../css/homepage.css';
import avto from '../img/bmw-i4-edrive-40-onepager-ms-range.jpg';
import uslugi from '../img/uslugi.jpg';
import Objevleniye from './objevleniye/objevleniye';

const HomePage = () => {
  return (
    <div>
      <main>
        <div className="container">
          <div className="row">
            <div className="mini_card_category">
            <button id="card_category" class="card_btn">
  Авто
  <img src={avtologo} id='auto' class="card_img" />
</button>
<button id="card_category" class="card_btn">
  Авто
  <img src={avtologo} id='auto' class="card_img" />
</button>
<button id="card_category" class="card_btn">
  Авто
  <img src={avtologo} id='auto' class="card_img" />
</button>
<button id="card_category" class="card_btn">
  Авто
  <img src={avtologo} id='auto' class="card_img" />
</button>
<button id="card_category" class="card_btn">
  Авто
  <img src={avtologo} id='auto' class="card_img" />
</button>
<button id="card_category" class="card_btn">
  Авто
  <img src={avtologo} id='auto' class="card_img" />
</button>
            </div>
        <div class="cars-container">
  <h2 class="cars-heading">Авто на рынке</h2>
  <div class="cars-grid">
    <a href={Objevleniye}>
    <div class="car-card">
      <img src={avto} class="car-img"/>
      <h3 class="car-title">Audi A4</h3>
      <p class="car-price">Цена: $20,000</p>
      <p class="car-location">Местоположение: Москва</p>
    </div>
      </a>
    <div class="car-card">
    <img src={avto} class="car-img"/>
      <h3 class="car-title">BMW 3 Series</h3>
      <p class="car-price">Цена: $25,000</p>
      <p class="car-location">Местоположение: Санкт-Петербург</p>
    </div>
    <div class="car-card">
    <img src={avto} class="car-img"/>
      <h3 class="car-title">Mercedes-Benz C-Class</h3>
      <p class="car-price">Цена: $30,000</p>
      <p class="car-location">Местоположение: Екатеринбург</p>
    </div>
  </div>
</div>
</div>
          </div>
<div class="services-container">
  <h2 class="services-heading">Сервисы и услуги</h2>
  <div class="services-grid">
    <div class="service">
    <img src={uslugi} class="service-img"/>
      <h3 class="service-title">Услуга 1</h3>
    </div>
    <div class="service">
    <img src={uslugi} class="service-img"/>
      <h3 class="service-title">Услуга 2</h3>
    </div>
    <div class="service">
    <img src={uslugi} class="service-img"/>
      <h3 class="service-title">Услуга 3</h3>
    </div>
  </div>
        </div>


      </main>
    </div>
  );
};

export default HomePage;
