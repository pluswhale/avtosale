import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import HomePage from './pages/homepage';
import Header from './header';
import AboutPage from './pages/aboutpage';
import Objevleniye from './pages/objevleniye/objevleniye'


const App = () => {
  return (
    <Router>
      <Header />
      <Routes>
        <Route path="/about" element={<AboutPage />} />
        <Route path="/home" element={<HomePage />} />
        <Route path="/objevleniye" element={<Objevleniye />} />
      </Routes>
    </Router>
  );
}

export default App;