
import React, { useEffect } from "react";

function CarBrands() {
  useEffect(() => {
    const url = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/car_brand";
    const token = "812e095da31a1f69fc965c2a6a82ede30617675a";
    const query = "форд";

    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: "Token " + token,
      },
      body: JSON.stringify({ query: query }),
    };

    fetch(url, options)
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        // Дополнительная обработка данных
      })
      .catch((error) => console.log("error", error));
  }, []);

}

export default CarBrands;
