import React from 'react';
import { Link } from 'react-router-dom';
import "./css/header.css";
import Modal from './modal/modal';
import CarBrands from './basa.js/bd';

const Header = () => {
  return (
    <header>
      <div className="header-container">
        <nav className="navigation">
          <ul>
            <li><Link to="/">Категория 1</Link></li>
            <li><Link to="/">Категория 2</Link></li>
            <li><Link to="/">Категория 3</Link></li>
            <li><Link to="/">Категория 4</Link></li>
          </ul>
        </nav>
        <div class="user-actions">
          <a href="/" id="login-link">Login</a>
          <a href="/" id="register-link">Register</a>
        </div>
        <Modal />
      </div>
      <div className="search-container">
        <div className="logo">
          <a href="/">Logo</a>
        </div>      
        <div className="dropdown">
          <button  className="avito-button">Все категории</button>
          <div id="myDropdown" className="dropdown-content">
            <a href="#">Категория 1</a>
            <a href="#">Категория 2</a>
            <a href="#">Категория 3</a>
          </div>
        </div>
        <div className="search-container_input">
          <CarBrands />
          <input type="text" id="search-input" placeholder="Поиск..." />
          <button id="search-button" className="avito-button">Найти</button>
        </div>
      </div>
    </header>
  );
};

export default Header;
